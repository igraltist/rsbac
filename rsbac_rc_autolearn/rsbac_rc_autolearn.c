#include <linux/init.h>
#include <linux/module.h>

#include <linux/device.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/uaccess.h>
#include <linux/string.h>
#include <linux/sched.h>
#include <linux/slab.h>         /* kzalloc */
#include <linux/jiffies.h>      /* jiffies */

#include "rsbac_rc_autolearn.h"

#define DEVICE_NAME "rsbac"
#define CLASS_NAME "rsbac"
#define AUTOLEARN_TTL 60

unsigned int major;                     /* holds the major number */
unsigned int opens = 0;                 /* counts the use of device was opened */
struct class *char_class = NULL;        /* for create_class() */
struct device *char_device = NULL;      /*  or create_device() */
int device;                             /* holds the MKDEV() */

/**
 * prototype functions must be defined before use in fops
 */
int device_open(struct inode *, struct file *);
int device_release(struct inode *inode, struct file *);
long device_ioctl(struct file *, unsigned int, unsigned long);

static const struct file_operations ioctl_fops = {
        .open           = device_open,
        .release        = device_release,
        .owner          = THIS_MODULE,
        .unlocked_ioctl = device_ioctl,
};

static int __init ioctl_init(void)
{
        printk(KERN_INFO "Module loading: rsbac_rc_autolearn\n");
        
        major = register_chrdev(0, DEVICE_NAME, &ioctl_fops);
        if (major < 0) {
                printk(KERN_ALERT "Failed to registering the character device %d\n", major);
                return major;
        }
        printk(KERN_INFO "The major number for your device is %d\n", major);
       
        char_class = class_create(THIS_MODULE, CLASS_NAME);
        if (IS_ERR(char_class)) {
                unregister_chrdev(major, DEVICE_NAME);
                printk(KERN_ALERT "Failed to registering device class\n");
                return PTR_ERR(char_class);
        }
        device = MKDEV(major, 0);
        char_device = device_create(char_class, NULL, device, NULL, DEVICE_NAME);
        if (IS_ERR(char_device)) {
                class_destroy(char_class);
                unregister_chrdev(major, DEVICE_NAME);
                printk(KERN_ALERT "Failed to create the device: %s\n", DEVICE_NAME);
                return PTR_ERR(char_device);
        }
        ioctl = kzalloc(sizeof(ioctl), GFP_KERNEL);
        if (!ioctl)
                return -ENOMEM;
        
        /* initialize some value */
        ioctl->role = -1;
        ioctl->old_role = -1;
        ioctl->ttl = -1;
        ioctl->cmd = -1;
        ioctl->fd_type = -1;
        ioctl->expires = 0;

        if (load_autolearn() != 0) 
               return -ENOEXEC;

        return 0;
}  
module_init(ioctl_init);

static void __exit ioctl_exit(void) 
{
        device_destroy(char_class, device);
        class_unregister(char_class);
        class_destroy(char_class);
        unregister_chrdev(major, DEVICE_NAME);
        kfree(ioctl);
        unload_autolearn();
       
        printk(KERN_INFO "Module removed: rsbac_rc_autolearn\n");
}  
module_exit(ioctl_exit);  

/*
 * only for stats in moment no other use 
 */
int device_open(struct inode *inode, struct file *filp)
{
        printk(KERN_INFO "Device open\n");
        opens++;
        printk(KERN_INFO "rsbac_rc_autolearn: Device has been opened %d time(s)\n", opens);
        return 0;
}

/*
 * only for info at moment
 */
int device_release(struct inode *inode, struct file *filp)
{
        printk(KERN_INFO "Device close\n");
        return 0;
}

/*
 * handle the userspace command from rc_autolearn
 */
long device_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
        long ret = 0;
        int err;
        struct ioctl_cmd_t ioctl_cmd;

        if (_IOC_TYPE(cmd) != IOC_MAGIC || _IOC_NR(cmd) > IOC_MAXNR) 
                return -ENOTTY;
        if (!access_ok(VERIFY_WRITE, (void __user *)arg, _IOC_SIZE(cmd)))
                return -EFAULT;
        err = copy_from_user(&ioctl_cmd, (void __user *)arg,
                             sizeof(struct ioctl_cmd_t));
        if (err != 0) 
                return -EFAULT;
        if (ioctl_cmd.role && (ioctl_cmd.role < 0 || ioctl_cmd.role > 999999)) 
                return -EPERM;
        if (ioctl_cmd.role != ioctl->role) 
                ioctl->role = ioctl_cmd.role;
        if (ioctl->role >= 0) 
                ioctl->old_role = ioctl->role;
        err= rsbac_rc_test_role_admin(TRUE);
        if (err == 0) 
                ioctl->admin = 1;
        else
                ioctl->admin = 0;
        ioctl->pid = task_pid_nr(current);
        ioctl->uid = get_current_user()->uid.val;

        /* 
         * set ioctl->cmd this is an exported symbold and used 
         * in request_func which handle the rsbac stuff
         */
        switch(cmd) {
        case IOCTL_BEGIN:
                if (ioctl_cmd.ttl == 0) 
                        ioctl->ttl = AUTOLEARN_TTL;
                else 
                        ioctl->ttl = ioctl_cmd.ttl;
                if (ioctl->admin == 1)
                        ioctl->expires = jiffies / HZ + ioctl->ttl;
                ioctl->cmd = 0;
                break;
        case IOCTL_END: 
                ioctl->cmd = 1;
                break;
        case IOCTL_INFO: 
                ioctl->cmd = 2;
                break;
        default:
                return -ENOTTY;
        } 
        
        return ret;
}

MODULE_AUTHOR("Jens Kasten"); 
MODULE_LICENSE("GPL"); 
MODULE_DESCRIPTION("RSBAC RC autolearn module for a single role.");
MODULE_VERSION("0.1");
