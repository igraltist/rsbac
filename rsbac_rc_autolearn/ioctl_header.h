#define IOC_MAGIC 'r' 

#define IOCTL_BEGIN _IOW(IOC_MAGIC, 0, int)
#define IOCTL_END _IOW(IOC_MAGIC, 1, int)
#define IOCTL_INFO _IOW(IOC_MAGIC, 2, int)

/* max ioctl */
#define IOC_MAXNR 3

/*
 * holds elements thats coming from userspace
 *
 * @role rc_role 
 * @ttl time to life
 * @fd_type rc_fd_types when set this are exclude
 */
struct ioctl_cmd_t {
        int role;
        int ttl;
        int fd_type;
};


#ifdef __KERNEL__
/*
 * @role rc_role to autolearn
 * @old_role previous roled
 * @ttl time to life for autolearn
 * @fd_type rc_fd_types when set this are exclude
 * @cmd command [begin,end,info]
 * @expires hold time when autolearn should end
 * @pid pid from userspace call
 * @uid uid from userspace call
 * @learning hold value as yes or no
 */
struct ioctl_t {
        int role;
        int old_role;
        int ttl;
        int fd_type;
        int cmd;
        unsigned long expires;        
        unsigned int pid;
        unsigned int uid;
        unsigned int learning;
        unsigned int admin;
}; 

struct ioctl_t *ioctl;
#endif

