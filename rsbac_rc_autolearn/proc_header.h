int load_proc_entries(void);
void unload_proc_entries(void);

#if defined(CONFIG_RSBAC_PROC)

#include <rsbac/proc_fs.h>

#define PROC_NAME "reg_rc_autolearn"

struct proc_dir_entry * reg_proc_p;

int reg_proc_show(struct seq_file *m, void *v);
int reg_proc_open(struct inode *inode, struct file *file);

static const struct file_operations reg_proc_fops = {
       .owner          = THIS_MODULE,
       .open           = reg_proc_open,
       .read           = seq_read,
       .llseek         = seq_lseek,
       .release        = single_release,
};

int load_proc_entries(void) 
{
        reg_proc_p = proc_create(PROC_NAME, S_IFREG|S_IRUGO, proc_rsbac_root_p, &reg_proc_fops);
        if (!reg_proc_p) {
                rsbac_printk(KERN_WARNING "%s: Not loaded due to failed proc entry registering.\n", PROC_NAME);
                return -ENOEXEC;
        }
        
        return 0;
}

void unload_proc_entries(void) 
{
        remove_proc_entry(PROC_NAME, proc_rsbac_root_p);
}

int reg_proc_show(struct seq_file *m, void *v)
{
        int err;
        union rsbac_target_id_t       rsbac_target_id;
        union rsbac_attribute_value_t rsbac_attribute_value;
        

        if (!rsbac_is_initialized())
                return -ENOSYS;

        rsbac_target_id.scd = ST_rsbac;
        rsbac_attribute_value.dummy = 0;
        err = rsbac_adf_request(R_GET_STATUS_DATA, task_pid(current), T_SCD,
                                rsbac_target_id, A_none, rsbac_attribute_value);
        if (!err) 
                return -EPERM;
        
        seq_puts(m, "RSBAC REG decision module rsbac_rc_autolearn\n----------------------------------\n");
        seq_printf(m, "%lu calls to request function.\n", nr_request_calls);
        seq_printf(m, "%lu calls to set_attr function.\n",nr_set_attr_calls);
        seq_printf(m, "%lu calls to need_overwrite function.\n", nr_need_overwrite_calls);
        seq_printf(m, "%lu calls to system_call function %lu, last arg was %p.\n",
                   nr_system_calls, syscall_dispatcher_handle, system_call_arg);

        return 0;
}

int reg_proc_open(struct inode *inode, struct file *file)
{
        return single_open(file, reg_proc_show, NULL);
}

#else
int load_proc_entries(void)
{
        return 0;
}
#endif
