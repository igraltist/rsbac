#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>


/* 8 bit number */
#define IOC_MAGIC 'r' 

/* write commands to the kernel */
#define IOCTL_BEGIN _IOW(IOC_MAGIC, 0, int)
#define IOCTL_END _IOW(IOC_MAGIC, 1, int)
#define IOCTL_INFO _IOW(IOC_MAGIC, 2, int)

void usage(char **);

struct ioctl_arg {
        int role;
        int ttl;
        long int fd_types;
};

int main(int argc, char *argv[])
{
        int fd, cmd = -1, fd_type = -1;
        
        struct ioctl_arg arg;
        
        while(1) {
                static struct option long_options[] = {
                        {"ttl", required_argument, 0, 't'},
                        {"role", required_argument, 0, 'r'},
                        {"exclude", required_argument, 0, 'r'},
                        {"help", no_argument, 0, 'h'},
                        {0, 0, 0, 0}
                };

                /* getopt_long stores index */
                int option_index = 0;

                int opt = getopt_long(argc, argv, "vbc:t:r:e:h", long_options, &option_index);

                /* detect end of options */
                if (opt == -1)
                        break;

                switch(opt) {
                        case 0:
                                if (long_options[option_index].flag != 0) 
                                        break;
                                printf("option %s", long_options[option_index].name);
                                if (optarg) 
                                        printf("with arg '%s'", optarg);
                                printf("\n");
                                break;
                        case 'c':
                                //printf("option --command, -c with '%s'\n", optarg);
                                cmd = atoi(optarg);
                                break;
                        case 't':
                                //printf("option --ttl, -t with '%s'\n", optarg);
                                arg.ttl = atoi(optarg);
                                break;
                        case 'r':
                                //printf("option --role, -r with '%s'\n", optarg);
                                arg.role = atoi(optarg);
                                break;
                        case 'f':
                                //printf("options --exclude, -e with '%s'\n", optarg);
                                fd_type = atoi(optarg);
                                break;
                        case '?':
                                break;
                        case 'h':
                                usage(argv);
                                exit(0);
                        default: 
                               abort(); 
                }

        }

        /* Print any remaining command line arguments (not options). */
        if (optind < argc) {
                /*
                printf("len argc: %d, optind: %d\n", argc, optind);
                printf("non-option ARGV-elements: ");
                */
                while (optind < argc) {
                        if (strncmp(argv[optind], "begin", strlen("begin")) == 0) {
                                cmd = 0;
                                break;
                        } else if (strncmp(argv[optind], "end", strlen("begin")) == 0) {
                                cmd = 1;
                                break;
                        } else if (strncmp(argv[optind], "info", strlen("info")) == 0) {
                                cmd = 2;
                                break;
                        }
                        /*
                        printf("%s ", argv[optind]);
                        */
                        optind++;
                }
                /*
                putchar('\n');
                */
        }

        if (cmd < 0 || cmd > 3) {
                usage(argv);
                exit(1);
        }
        if (arg.role < 0 || arg.role > 999999) {
                usage(argv);
                printf("  Error: role_number is missing\n");
                exit(1);
        }
        
        printf("cmd: %d, role: %d, ttl: %d, exclude: %d\n", 
                cmd, arg.role, arg.ttl, fd_type);
        

        fd = open("/dev/rsbac", O_RDWR);
        if (fd == -1) {
                printf("Error in opening file \n");
                exit(-1);
        }
        switch(cmd) {
        case 0:
                ioctl(fd, IOCTL_BEGIN, &arg);
                break;
        case 1:
                ioctl(fd, IOCTL_END, &arg);
                break;
        case 2:
                ioctl(fd, IOCTL_INFO, &arg);
                break;
        }
        close(fd);

        return EXIT_SUCCESS;
}

void usage(char *argv[]) 
{
        printf("Usage: %s [begin|end,info] [OPTIONS]\n", argv[0]);
        printf("  -r --role     role_number to autolearn\n");
        printf("  -t --ttl      time-to-life in seconds for autolearn\n");
        printf("  -e --exclude  numbers of rc_fd_types to exclude separeted by coma\n");
        printf("  -h, --help    print this help and exit\n");
        printf("\n");
}


