#include <linux/unistd.h>

#include <rsbac/types.h>
#include <rsbac/reg.h>
#include <rsbac/adf.h>
#include <rsbac/aci.h>
#include <rsbac/getname.h>
#include <rsbac/error.h>
#include <rsbac/rc.h>
#include <rsbac/network.h>
#include <rsbac/rc_getname.h>
#include <rsbac/aci_data_structures.h>
#include <rsbac/rkmem.h>
#include <rsbac/helpers.h>

#include "ioctl_header.h"

#define REG_NAME "rc_autolearn"

static long handle = 9999990;
static u_long nr_request_calls = 0;
static u_long nr_set_attr_calls = 0;
static u_long nr_need_overwrite_calls = 0;
static u_long nr_system_calls = 0;

static void * system_call_arg = NULL;
static char * syscall_name = NULL;
static long syscall_registration_handle = 9999991;
static long syscall_dispatcher_handle = 1;
int do_learning = 0;

#include "proc_header.h"

int syscall_func(void *arg);
int load_autolearn(void);
int unload_autolearn(void);

enum rsbac_rc_item_t i_rc_item;

extern int rsbac_rc_test_admin_roles(rsbac_rc_role_id_t t_role,
                                     rsbac_boolean_t modify);
extern int rsbac_rc_test_role_admin(rsbac_boolean_t modify);

int syscall_func(void *arg)
{
        nr_system_calls++;
        system_call_arg = arg;

        return nr_system_calls;
}

static int request_func(enum  rsbac_adf_request_t     request,
                        rsbac_pid_t                   owner_pid,
                        enum  rsbac_target_t          target,
                        union rsbac_target_id_t       tid,
                        enum  rsbac_attribute_t       attr,
                        union rsbac_attribute_value_t attr_val,
                        rsbac_uid_t                   owner)
{
        int err;
	union rsbac_target_id_t i_tid;
	enum rsbac_attribute_t i_attr;
	union rsbac_attribute_value_t i_attr_val1;
	union rsbac_attribute_value_t i_attr_val2;

        union rsbac_rc_target_id_t i_rc_tid;
        union rsbac_rc_item_value_t i_rc_value;
	union rsbac_rc_target_id_t i_rc_subtid;
        rsbac_pid_t caller_pid;

        if (ioctl->role == -1) 
            return DO_NOT_CARE;    
        if (ioctl->expires <= (jiffies / HZ)) {
                printk(KERN_INFO "role %d expired\n", ioctl->role);
                ioctl->role = -1;
                ioctl->expires = 0;
                ioctl->learning = 0;
                return DO_NOT_CARE;
        }

        nr_request_calls++;

        switch (ioctl->cmd) {
        case 0: 
                ioctl->cmd = -1;
                if (ioctl->admin == 0) 
                        DO_NOT_CARE;
                break;
        case 1: 
                ioctl->expires = 0;
                ioctl->cmd = -1;
                return DO_NOT_CARE;
        case 2:
                if (ioctl->expires > 0)
                        printk(KERN_INFO "role %d expires in %lu second(s)\n", 
                              ioctl->role, (ioctl->expires - (jiffies / HZ)));
                ioctl->cmd = -1;
                return DO_NOT_CARE;
        default:
                ioctl->cmd = -1;
                return DO_NOT_CARE;
        }

        i_rc_tid.role = ioctl->role;
        caller_pid = (rsbac_pid_t) ioctl->pid;

        /* get rc_role from process */
	tid.process = caller_pid;
        err = rsbac_get_attr(SW_RC, T_PROCESS, tid, A_rc_role, &attr_val,
                        FALSE);
        printk(KERN_INFO "role from process err: %d\n", err);
        if (err) {
                printk(KERN_WARNING "no rc_role from process: %d\n", 
                        ioctl->pid);
		rsbac_pr_get_error(A_rc_role);
		return DO_NOT_CARE;
	}

        switch(target) {
        case T_UNIXSOCK:
       		i_rc_item = RI_type_comp_fd;
		i_attr = A_rc_type_fd;
		break;
	case T_DEV:
		i_rc_item = RI_type_comp_dev;
		i_attr = A_rc_type;
		break;
	case T_USER:
		i_rc_item = RI_type_comp_user;
		i_attr = A_rc_type;
		break;
	case T_PROCESS:
		i_rc_item = RI_type_comp_process;
		i_attr = A_rc_type;
		break;
	case T_IPC:
		i_rc_item = RI_type_comp_ipc;
		i_attr = A_rc_type;
		break;
#if defined(CONFIG_RSBAC_RC_UM_PROT)
	case T_GROUP:
		i_rc_item = RI_type_comp_group;
		i_attr = A_rc_type;
		break;
#endif
#if defined(CONFIG_RSBAC_RC_NET_DEV_PROT)
	case T_NETDEV:
		i_rc_item = RI_type_comp_netdev;
		i_attr = A_rc_type;
		break;
#endif
#if defined(CONFIG_RSBAC_RC_NET_OBJ_PROT)
	case T_NETTEMP:
		i_rc_item = RI_type_comp_nettemp;
		i_attr = A_rc_type_nt;
		break;
	case T_NETOBJ:
		i_rc_item = RI_type_comp_netobj;
		if (rsbac_net_remote_request(request))
			i_attr = A_remote_rc_type;
		else
			i_attr = A_local_rc_type;
		break;
#endif
	default:
		rsbac_printk(KERN_WARNING 
                        "request_func(): invalid target %i!\n", target);
		return DO_NOT_CARE;
	}
       
        printk(KERN_INFO "target: %i, rc_item %d, attr %d\n", target, i_rc_item, i_attr);

        err = rsbac_rc_get_item(0, RT_ROLE, i_rc_tid, i_rc_subtid,
                        i_rc_item, &i_rc_value, NULL);
        printk(KERN_INFO "get_item err: %d\n", err);
        if (err == 0) 
                ioctl->learning = 1;

        return GRANTED;
}

static int set_attr_func(enum rsbac_adf_request_t      request,
                         rsbac_pid_t                   owner_pid,
                         enum  rsbac_target_t          target,
                         union rsbac_target_id_t       tid,
                         enum  rsbac_target_t          new_target,
                         union rsbac_target_id_t       new_tid,
                         enum  rsbac_attribute_t       attr,
                         union rsbac_attribute_value_t attr_val,
                         rsbac_uid_t                   owner)
{
        int err;
        char *tmp, *tmp2;
	union rsbac_target_id_t i_tid;
	enum rsbac_attribute_t i_attr;
	union rsbac_attribute_value_t i_attr_val1;
	union rsbac_attribute_value_t i_attr_val2;
        union rsbac_rc_target_id_t i_rc_tid;
        union rsbac_rc_item_value_t i_rc_value;
	union rsbac_rc_target_id_t i_rc_subtid;
	enum rsbac_rc_item_t i_rc_item;
        rsbac_pid_t caller_pid;

        if (ioctl->learning == 0) 
                return 0;

        nr_set_attr_calls++;

        tid.process = (rsbac_pid_t) ioctl->pid;
        callerpid = (rsbac_pid_t) ioctl->pid;
        tmp = rsbac_kmalloc(RSBAC_MAXNAMELEN);
        if (!tmp) {
                ioctl->learning = 0;
                return -1;
        }
        tmp2 = rsbac_kmalloc(RSBAC_MAXNAMELEN);
        if (!tmp2) {
                ioctl->learning = 0;
                return -1;
        }
              
        i_rc_subtid.type = attr_val.rc_type;
        // check rsbac_rc_check_comp -> rc_main.c -> line 128
         
        i_rc_value.rights |= RSBAC_RC_RIGHTS_VECTOR(request);
        err = rsbac_rc_set_item (0, RT_ROLE, i_rc_tid,
                i_rc_subtid, i_rc_item, i_rc_value, RSBAC_LIST_TTL_KEEP);
        
        if (!err) {
               rsbac_printk(KERN_INFO 
                        "check_comp_rc(): learning mode: pid %u(%s), owner %u, rc_role %u, %s rc_type %u, right %s added to transaction %u!\n",
                        pid_nr(caller_pid), current->comm,  
                        __kuid_val(current_uid()), i_attr_val1.rc_role,
                        get_target_name_only(tmp, target),
                        i_attr_val2.rc_type,
                        get_rc_special_right_name(tmp2, request),
                        0);
                rsbac_kfree(tmp2);
                rsbac_kfree(tmp);
        }

        do_learning = 0;

        return 0;
}

static rsbac_boolean_t need_overwrite_func(struct dentry * dentry_p)
{
        nr_need_overwrite_calls++;
    
        return FALSE;
}

 
/*
 * load the rsbac reg module 
 * keep it outside from module init
 */
int load_autolearn(void) 
{
        struct rsbac_reg_entry_t entry;
        struct rsbac_reg_syscall_entry_t syscall_entry;
        int err; 

        err = load_proc_entries();
        if (err) 
                return -ENOEXEC;

        /* clearing registration entries */
        memset(&entry, 0, sizeof(entry));
        memset(&syscall_entry, 0, sizeof(syscall_entry));

        strncpy(entry.name, REG_NAME, RSBAC_REG_NAME_LEN);
        entry.handle = handle;
        entry.request_func = request_func;
        entry.set_attr_func = set_attr_func;
        entry.need_overwrite_func = need_overwrite_func;
        entry.switch_on = TRUE;
        err = rsbac_reg_register(RSBAC_REG_VERSION, entry);
        if (err < 0) {
                rsbac_printk(KERN_WARNING 
                        "RSBAC REG decision module %s: Registering failed. Unloading.\n",
                        REG_NAME);
                return -ENOEXEC;
        }

        strcpy(syscall_entry.name, "REG rc_autolearn syscall");
        syscall_entry.registration_handle = syscall_registration_handle;
        syscall_entry.dispatcher_handle = syscall_dispatcher_handle;
        syscall_entry.syscall_func = syscall_func;
        printk(KERN_INFO "RSBAC REG decision module %s: Registering syscall.\n",
                REG_NAME);
        syscall_registration_handle = rsbac_reg_register_syscall(RSBAC_REG_VERSION, 
                                                                 syscall_entry);
        if (syscall_registration_handle < 0) {
                printk(KERN_WARNING 
                        "RSBAC REG decision module %s: Registering syscall failed. Unloading.\n",
                        REG_NAME);
                if (rsbac_reg_unregister(handle)) {
                        printk(KERN_ERR 
                                "RSBSAC REG decision module %s: Unregistering failed - beware of possible system failure!\n",
                                REG_NAME);
                }
                return -ENOEXEC;
        }

        rsbac_printk(KERN_INFO "RSBAC REG decision module %s: Loaded.\n", 
                REG_NAME);
               
        return 0;
}

/*
 * unload the rsbac reg module
 * keep it outside form moduel exit
 */
int unload_autolearn(void)
{
        unload_proc_entries();

        if (rsbac_reg_unregister_syscall(syscall_registration_handle)) {
                rsbac_printk(KERN_ERR 
                        "RSBAC REG decision module %s: Unregistering syscall failed - beware of possible system failure!\n",
                        REG_NAME);
        }
        if (rsbac_reg_unregister(handle)) {
                rsbac_printk(KERN_ERR 
                        "RSBAC REG decision module %s: Unregistering failed - beware of possible system failure!\n"
                        REG_NAME);
                return -ENOEXEC;
        }

        rsbac_printk(KERN_INFO "RSBAC REG decision module %s: Unloaded.\n",
                REG_NAME);

        return 0;
}
